# Mastodon maintenance scripts

## About

インスタンスをメンテナンスするためのスクリプトたち
- clean-up.sh
  メディアをおそうじする

## Required

- docker-compose
- [glynnbird/toot](https://github.com/glynnbird/toot)
    - recommend: `npm install -g toot`

## License

[WTFPL](http://www.wtfpl.net/)

## Original Author

[@tsukiyonomegumi](https://gitlab.com/tsukiyonomegumi)

