#!/bin/bash
export PATH=$PATH:/snap/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

CLEAN_DAYS=14

echo "DATA CLEANUP START." | toot --visibility unlisted

cd ../mastodon

# media
result=`docker-compose run --rm web bundle exec bin/tootctl media remove --days=${CLEAN_DAYS}  | awk 'END{print $5,$6}'`

echo -e "DATA CLEANUP DONE.\n${result}" | toot --visibility unlisted
